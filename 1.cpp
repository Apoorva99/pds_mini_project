#include<bits/stdc++.h>
using namespace std;
 
int main()
{
    int n;
    cin>>n;
    //tag should be even as same number of closing and opening tag.
    if(n%2!=0)
        cout<<"The XML Tags should be in even number\n";
    //tag should not be 0
    else if(n==0)
        cout<<"The XML Tags count should be greater than zero\n";
    else{
        string tags[n];
        for(int i=0;i<n;i++){
            cin>>tags[i];
        }
        for(int i=0;i<n;i++){
            //XML tag should starts with '<' and ends with '>'
            if(tags[i][0]!='<' || tags[i][tags[i].length()-1]!='>'){
                cout<<"please enter a valid XML tag\n";
                return -1;
            }
            //if no text inside tags
            if((tags[i][1]=='/' && tags[i].length() <=3) || (tags[i][1]!='/' && tags[i].length() <=2)){
                cout<<"please enter a valid XML tag\n";
                return -1;
            }
 
        }
 
        char endTag = '/';
        size_t found;
 
        //stack will store the opening tags. the child tag will appear at the top while the parent tag will appear after the child tag.
        stack<string> st;
        for(int i=0;i<n;i++){
            //for finding the index of '/' in the string
            found = tags[i].find(endTag);
 
            if(found != string::npos){
                //its a closing tag
 
                //if no opening tags entered so far or all opened tag has been closed.
                if(st.size()==0){
                    cout<<"Please Type the opening tag before the closing tag for your XML code\n";
                    return -1;
                }
                //to get the last entered opening tag
                string openingString = st.top();
                string closingString = tags[i];
 
                //closing tag and opening tag size should differ only by 1 length.
                if(closingString.size()-openingString.size() != 1){
                    cout<<"Please Type the correct closing tag for the XML code\n";
                    return -1;
                }
 
                //check whether the opening and closing tag are of same tag are not
                for(int i=1;i<openingString.size()-1;i++){
                    if(openingString[i] != closingString[i+1]){
                        cout<<"Please Type the correct closing tag for the XML code\n";
                        return -1;
                    }
                }
                //all test case verified, so removing the last entered openong tag from the stack
                st.pop();
 
            }else{
                //its an opening tag
                st.push(tags[i]);
            }
        }
        //if less number of closing tags than the opening tag entered
        if(st.size()>0){
            cout<<"Please Type all the closing tag for the XML code\n";
            return -1;
        }
        cout<<"XML Tags are correctly entered\n";
    }
}
