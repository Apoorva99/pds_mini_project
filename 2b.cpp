#include<bits/stdc++.h>
using namespace std;
 
struct Node {
    int data;
    Node* left;
    Node* right;
};
 
Node* newNode(int data){
    Node* tempNode = new Node();
    tempNode -> data = data;
    tempNode -> left = tempNode -> right = NULL;
    return tempNode;
}
 
int findMaxDepthByRecursion(Node* root){
    if(root==NULL)
        return 0;
   
    //check the left subtree and right subtree depth for each node and select the maximum of them and 1 for each node
    return max(findMaxDepthByRecursion(root->left), findMaxDepthByRecursion(root->right)) + 1;
}
 
 
 
int findMaxDepthByIteration(Node* root){
    //if the tree is empty
    if(root==NULL)
        return 0;
   
    //initialize a queue data structure to hold all the nodes
    queue<Node *> q;
    q.push(root);
 
    //initialize the maximum depth by 0
    int maxDepth = 0;
 
    //Run a while loop till all the elements from the queue become empty
    while(!q.empty()){
        int totalNodes = q.size();
        maxDepth++;
 
        //Dequeue the current level node and enqueue the childs of that node i.e the next level nodes
        while (totalNodes > 0){
            Node* temp = q.front();
 
            //pop the front node
            q.pop();
           
            //push the left child of temp node in queue if it is not null
            if(temp->left!=NULL)
                q.push(temp->left);
           
            //push the left child of temp node in queue if it is not null
            if(temp->right!=NULL)
                q.push(temp->right);
           
            //decrease the total count of node at the temp's Node level
            totalNodes--;
        }
    }
    //return the count of maximum depth
    return maxDepth;
}
 
 
int main()
{
    Node* root = newNode(10);
    root->left = newNode(11);
    root->left->left = newNode(7);
    root->right = newNode(9);
    root->right->left = newNode(15);
    root->right->right = newNode(8);
   
    //calculating the maximum depth of Binary Tree by recursive algorithms
    int maximumDepthByRecursion = findMaxDepthByRecursion(root);
    cout<<"The Maximum depth in the given binary tree is : "<<maximumDepthByRecursion<<endl;
 
    //calculating the maximum depth of Binary Tree by non recursive algorithms
    int maximumDepthByIteration = findMaxDepthByIteration(root);
    cout<<"The Maximum depth in the given binary tree is : "<<maximumDepthByIteration;
 
}
