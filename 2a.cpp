#include<bits/stdc++.h>
using namespace std;
 
struct Node {
    int data;
    Node* left;
    Node* right;
};
 
Node* currentNode(int data){
    Node* tempNode = new Node();
    tempNode -> data = data;
    tempNode -> left = tempNode -> right = NULL;
    return tempNode;
}
 
int countAllNodeRecursive(Node* root){
    //if root is null
    if(root==NULL)
        return 0;
 
    //otherwise go to each node and increment the count
    return(countAllNodeRecursive(root->left) + 1 + countAllNodeRecursive(root->right));
}
 
int countAllNodeIteration(Node* root){
    //if tree is empty
    if(root==NULL)
        return 0;
   
    //initialize a queue data structure to hold all the nodes.
    queue<Node*> q;
    q.push(root);
 
    //initialize the count of node to 0
    int count = 0;
 
    //Run a while loop till all the elements from the queue become empty
    while(!q.empty()){
        //get the front node from queue
        Node* temp = q.front();
 
        //pop the front element from queue
        q.pop();
 
        //increment the count of node
        count++;
 
        //push node into queue if it has left child
        if(temp->left!=NULL)
            q.push(temp->left);
 
        //push node into queue if it has left child
        if(temp->right!=NULL)
            q.push(temp->right);
       
    }
    return count;
 
}
 
int main()
{
    Node* presentRoot = currentNode(19);
    presentRoot->left = currentNode(1);
    presentRoot->left->left = currentNode(76);
    presentRoot->right = currentNode(29);
    presentRoot->right->left = currentNode(25);
    presentRoot->right->right = currentNode(81);
   
    //calculating the total nodes of above binary tree by recursive algorithms
    int totalNodes = countAllNodeRecursive(presentRoot);
    cout<<"Total nodes in the given binary tree are : "<<totalNodes<<endl;
 
    ////calculating the total nodes of above binary tree by non-recursive(iteration) algorithms
    int totalNodesByIteration = countAllNodeIteration(presentRoot);
    cout<<"Total nodes in the given binary tree are : "<<totalNodesByIteration<<endl;
}
